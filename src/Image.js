import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
const useStyles = makeStyles({
    avatar: {
        margin: 10,
    },
    bigAvatar: {
        margin: 10,
        width: 60,
        height: 60,
    },
});

export default function Upload() {
    const classes = useStyles();
    return (
        <React.Fragment>
            <h1> Une image</h1>

            <Grid container justify="left" alignItems="left">
                <Avatar alt="Remy Sharp" src="https://pbs.twimg.com/profile_images/1180026194/picto-onde-orange_400x400.gif" className={classes.bigAvatar} />
            </Grid>

        </React.Fragment>
    );
}
