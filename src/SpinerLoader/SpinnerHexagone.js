import React from 'react';
import Container from '@material-ui/core/Container';
import HexagoneSpinner from './HexagoneSpinner.png'
import './Spinner.css';
function SpinnerHexagone() {

    return (
        <Container className="containerPrincipalSpinner">
            <Container className="containerSecondaireSpinner">
                <img
                 className="hexagoneSpinner"
                 src={HexagoneSpinner}
                />
            </Container>
        </Container>

    )

};

export default SpinnerHexagone; 