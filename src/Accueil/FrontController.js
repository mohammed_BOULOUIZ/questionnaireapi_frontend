import React, { useState } from 'react';
import Axios from 'axios';
import ContentData from './ContentData';
import SpinnerHexagone from '../SpinerLoader/SpinnerHexagone';

export default function FrontController(props) {
    const [dataAxios, setDataAxios] = React.useState();
    const [prevSelection, setPrevSelection] = React.useState();
    const [urlHost, setUrlHost] = React.useState('http://localhost:8000');
    const urlList = urlHost + props.selectionSwitch;
    const [datatId, setDataId] = React.useState();
    const [action, setAction] = React.useState();
    const [valuesCrt, setValuesCrt] = React.useState();
    const [isLoading, setIsLoading] = React.useState(false);
    const [reload, setReload] = React.useState(false);
    const urlRest = urlList.concat(datatId)

    function RecievId(id) {
        setDataId(id)
    }

    function ReceiveAction(action) {
        setAction(action)
    }

    function ReciveValues(values) {
        setValuesCrt(values)
    }

    function receiveCreate(action) {
        setAction(action)
    }

    function onCancel() {
        setAction();
        setDataId();
        setValuesCrt();
    }

    function onLoad(boolean){
        setIsLoading(boolean)
    }
    /**************************************************************************************
    ******************** READ
    **************************************************************************************/
    if (props.selectionSwitch !== prevSelection) {
        setPrevSelection(props.selectionSwitch)
        onCancel()
        Axios.get(urlList).then(response => {
            console.log(response);
            const data = response.data
            setDataAxios(data)
        });
        setIsLoading(props.isLoading)
    }

    /**************************************************************************************
    ******************** CREATE
    **************************************************************************************/
    if (action && action === 'Create' && valuesCrt) {

        Axios.post(urlList, valuesCrt)
                 .then(response => {
                        console.log(response);
                        setReload(true);
                        setIsLoading(true)
                        if (response.data){
                            console.log(response.data[0].message)
                        }
                })            

            setValuesCrt();
    }
    

    /**************************************************************************************
    ******************** DELETE
    **************************************************************************************/
    if (action && action === 'Delete') {
        Axios.delete(urlList + datatId)
            .then(response => {
                console.log(response);
                setReload(true);
                setIsLoading(true);})
            onCancel();    
                   


    } else
        /**************************************************************************************
        ******************** UPDATE
        **************************************************************************************/
        if (action && action === 'Update' && valuesCrt) {
            Axios.put(urlRest, valuesCrt)
                .then(response => {
                    console.log(response);
                    setReload(true);
                    setIsLoading(true)
                })
                setValuesCrt();
                onCancel();           

        } else {
            console.log('Sorry, ya rien');
        }
    /**************************************************************************************
    ******************** RELOAD AFTER CREATE, DELETE, UPDATE
    **************************************************************************************/
    if (reload) {

        Axios.get(urlList).then(response => {
            const data = response.data
            setDataAxios(data);
            console.log(response);
        });
        setReload(false);
    }


    if (!isLoading){
    return (<SpinnerHexagone />
        
    )}else{
        return (
            <ContentData selectionSwitch={props.selectionSwitch} dataContent={dataAxios} urlList={urlList}
            RecievId={RecievId} ReceiveAction={ReceiveAction} ReciveValues={ReciveValues} receiveCreate={receiveCreate}
            action={action} datatId={datatId} onCancel={onCancel} onLoad={onLoad}/>
        )
    }

}