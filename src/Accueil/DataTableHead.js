import React, { useState } from 'react';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';


export default function DataTableHead(props) {
    const dataProps = props.rowsAttribute;
    const attributes = dataProps && dataProps.map(data => Object.getOwnPropertyNames(data))

        return (
            <TableHead>

                <TableRow className="text-center">
                    {attributes[0] && attributes[0].map(attribute =>
                        (attribute !== 'region' && /*attribute !== 'typereponse' && pour exclure*/
                            <TableCell component="td" scope="row" key={attribute}>{
                                (attribute && attribute !== 'region' && attribute && attribute !== 'image')
                                && attribute}</TableCell>
                        )
                    )
                    }
                    <TableCell component="td" scope="row">Supprimer</TableCell>
                    <TableCell component="td" scope="row">Modifier</TableCell>
                </TableRow>
            </TableHead>)
    
   
}