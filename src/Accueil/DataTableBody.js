import React, { useState } from 'react';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import IconButton from '@material-ui/core/IconButton';
import Delete from '@material-ui/icons/Delete';
import Edit from '@material-ui/icons/Edit';



export default function DataTableBody(props) {
    const dataProps = props.rowsData;
    const attributes = dataProps && dataProps.map(data => Object.getOwnPropertyNames(data))
    const attributesBody = attributes && attributes[0].map(attributeBody => attributeBody)   
    function SentId(id, boolean) {
        if (boolean){
            props.TransitId(id)
            props.TransitLoad(boolean)
        }else{
            props.TransitId(id)
        }
    }
    function SentAction(action){
        props.TransitAction(action)
    }
    return (
    <TableBody>
        {dataProps && dataProps.map(data =>
            <TableRow >
                {attributesBody && attributesBody.map(attributeBody => (
                    (attributeBody ) &&  /*(attributeBody !== 'typereponse') && pour exclure*/  ( 
                        <TableCell component="td" scope="row" key={data.attributeBody}>{data[attributeBody]}</TableCell>
                    )
                ))}
                
                <TableCell component="td" scope="row" > <IconButton align="left" 
                    onClick={() => { if (window.confirm('Vous allez supprimer ID= '+data.id+' etes vous sur ?'))
                    SentId(data.id, false) ; SentAction('Delete') }   }> <Delete /> </IconButton>  
                </TableCell>
                <TableCell component="td" scope="row" > <IconButton align="left" 
                    onClick={() => { SentId(data.id) ; SentAction('Update')}} > <Edit /> </IconButton>    
                </TableCell>
            </TableRow>
        )}
    </TableBody>
    )
}