import React, { useState } from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import Grid from '@material-ui/core/Grid';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Forum from '@material-ui/icons/Forum';
import Public from '@material-ui/icons/Public';
import Photo from '@material-ui/icons/Photo';
import Spellcheck from '@material-ui/icons/Spellcheck';
import Feedback from '@material-ui/icons/Feedback';
import Palette from '@material-ui/icons/Palette';


// import Contenu from './Contenu';

import FrontController from './FrontController';


const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
}));



export default function Home() {
    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);
    const [selection, setSelection] = useState('');
    const [isLoading, setIsLoading] = React.useState();



    function handleDrawerOpen() {
        setOpen(true);
    }

    function handleDrawerClose() {
        setOpen(false);
    }

    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="Open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, {
                            [classes.hide]: open,
                        })}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap>
                        Gestion des questionnaires
          </Typography>
                </Toolbar>
            </AppBar>
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                })}
                classes={{
                    paper: clsx({
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    }),
                }}
                open={open}
            >
                <div className={classes.toolbar}>
                    <IconButton onClick={handleDrawerClose}>
                        {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
                    </IconButton>
                </div>
                <Divider />

                <ListItem button onClick={() => {setSelection('/Questionnaire/'); setIsLoading(true)}} >
                    <ListItemIcon>  <Forum /> </ListItemIcon>
                    <ListItemText primary="Questionnaire" className={classes.button} />
                </ListItem>
                <ListItem button onClick={() => {setSelection('/Question/'); setIsLoading(true)}} >
                    <ListItemIcon>  <Feedback /> </ListItemIcon>
                    <ListItemText primary="Question" className={classes.button} />
                </ListItem>
                <Divider />
                <ListItem button onClick={() => {setSelection('/Theme/'); setIsLoading(true)}} >
                    <ListItemIcon> <Palette />  </ListItemIcon>
                    <ListItemText primary="Theme" className={classes.button} />
                </ListItem>
                <ListItem button onClick={() => {setSelection('/TypeReponse/');setIsLoading(true)}} >
                    <ListItemIcon>  <Spellcheck /> </ListItemIcon>
                    <ListItemText primary="Type" className={classes.button} />
                </ListItem>
                <Divider />
                <ListItem button onClick={() => {setSelection('/Region/');setIsLoading(true)}} >
                    <ListItemIcon>  <Public /> </ListItemIcon>
                    <ListItemText primary="Region" className={classes.button} />
                </ListItem>
                <ListItem button onClick={() => {setSelection('/Image/'); setIsLoading(true)}} >
                    <ListItemIcon>  <Photo />  </ListItemIcon>
                    <ListItemText primary="Image" className={classes.button} />
                </ListItem>
            </Drawer>
            <main className={classes.content}>
                <div className={classes.toolbar} />
                <div className={classes.root}>
                    <Grid container spacing={0}>
                        <Grid item xs={12} sm={12} >
                            <FrontController selectionSwitch={selection} isLoading={isLoading}/>
                        </Grid>
                    </Grid>
                </div>
            </main>
        </div>
    );
}
