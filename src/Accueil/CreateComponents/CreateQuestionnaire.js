import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import Box from '@material-ui/core/Box';
import useForm from "./useForm";
import Edit from '@material-ui/icons/Edit';
import Backspace from '@material-ui/icons/Backspace';

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },

    margin: {
        margin: theme.spacing(1),
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));
export default function CreateQuestionnaire(props) {
    const [editing, setEditing] = React.useState(false);
    const [questionnaire, setQuestionnaire] = React.useState({
        nomQuestionnaire: ''
    });
    const { handleChange, handleSubmit } = useForm();
    const classes = useStyles();

    function PushValues(values) {
        props.SentValues(values)
    }

    function PushValues(values, boolean) {
        props.SentValues(values);
        props.SentLoad(boolean)
        setEditing(false)
    }

    function Create(action) {
        props.SentCreate(action)
        setQuestionnaire({
            nomQuestionnaire: ''
        })
    }



    function onCancel() {
        setQuestionnaire({
            nomQuestionnaire: ''
        })
        props.onCancel()
        setEditing(false)
    }

    function getQuestionnaire(id) {
        try {
            Axios.get(props.urlList.concat(id))
                .then(response => {
                    console.log('nom theme ' + response.data.nomTheme)
                    setQuestionnaire(response.data)
                });
        } catch (error) {
            console.log(error.response)
        }
    }
    useEffect((e) => {
        if (props.dataId !== null, props.action === "Update") {
            setEditing(true);
            getQuestionnaire(props.datatId)
        }

    }, [props.action] && [props.datatId]);

    return (
        <React.Fragment>
            {!editing && <h2>Créer un Questionnaire</h2> || <h2>Editer le questionnaire N° {props.datatId}</h2>}
            <form className={classes.root} autoComplete="on" onSubmit={handleSubmit}>
                <FormControl id="myform" >
                    <div>
                        <Box component="div" display="inline" p={1} m={1}>
                            <Input id="my-input" aria-describedby="my-helper-text" placeholder="nom du quesitonnaire" name='nomQuestionnaire'
                                onChange={handleChange(setQuestionnaire, questionnaire)} value={questionnaire.nomQuestionnaire} />
                        </Box>

                        <Box component="div" display="inline" p={1} m={1}>

                            {!editing &&
                                <Fab type="submit" size="small" color="primary" aria-label="Add" className={classes.margin}
                                    onClick={() => { PushValues(questionnaire, false); Create('Create'); }}  > <AddIcon /> </Fab>
                                ||
                                <Box component="div" display="inline" p={1} m={1}>
                                    <Fab type="submit" size="small" color="secondary" aria-label="Add" className={classes.margin}
                                        onClick={() => {
                                            if (window.confirm('Vous allez modifier ID= ' + props.datatId + ' etes vous sur ?')) {
                                                PushValues(questionnaire, false);
                                            } else { onCancel() }
                                        }}> <Edit /> </Fab>

                                    <Fab type="submit" size="small" color="secondary" aria-label="Add" className={classes.margin}
                                        onClick={() => { onCancel(); setEditing(false) }}> <Backspace /> </Fab>
                                </Box>
                            }
                        </Box>
                    </div>
                </FormControl>
            </form>
        </React.Fragment>
    )
}