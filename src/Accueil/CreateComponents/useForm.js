function useForm() {

  function handleSubmit(event) {
      event && event.preventDefault();
  };

  const handleChange = (func, obj) => (event) => {
    func({...obj, [event.target.name]: event.target.value});
  };
  
  const handleUpload = (funcUpd, obj, e) => {
    funcUpd({...obj, [e.target.name]: e.target.files[0].name});
  };


  return {
    handleChange,
    handleUpload,
    handleSubmit
  }
};

export default useForm;