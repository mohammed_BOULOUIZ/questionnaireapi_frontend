import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Edit from '@material-ui/icons/Edit';
import Backspace from '@material-ui/icons/Backspace';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import useForm from "./useForm";
import Axios from 'axios';


const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },

    margin: {
        margin: theme.spacing(1),
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    button: {
        margin: theme.spacing(1),
    },
    input: {
        display: 'none',
    },
}));
export default function CreateImage(props) {
    const { handleChange, handleUpload, handleSubmit } = useForm();
    const classes = useStyles();
    const [image, setImage] = React.useState({ nomImage: '', urlImage: '' });
    const [editing, setEditing] = React.useState(false);
    const [file, setFile] = useState('');

    function PushValues(values, boolean) {
        props.SentValues(values);
        props.SentLoad(boolean)
        setEditing(false)
    }

    function Create(action) {
        props.SentCreate(action)
        setImage({
            nomImage: '',
            urlImage: ''
        })
    }


    function onCancel() {
        setImage({
            nomImage: '',
            urlImage: ''
        })
        props.onCancel()
        setEditing(false)
    }

    function getImage(id) {
        try {
            Axios.get(props.urlList.concat(id))
                .then(response => {
                    console.log('nom image ' + response.data.nomImage)
                    setImage(response.data)
                });
        } catch (error) {
            console.log(error.response)
        }
    }

    useEffect((e) => {
        if (props.dataId !== null && props.action === "Update") {
            setEditing(true);
            getImage(props.datatId)
        }

    }, [props.action] && [props.datatId]);


    function UpLoad(event) {
        setFile(event.target.files[0])
    }

    function SentUpLoadFile() {
        const formData = new FormData();
        formData.append('file', file);
        Axios.post('http://localhost:8000/upLoad/', formData)
            .then(response => { console.log("result : ", response) });
    }

    return (
        <React.Fragment>
            {!editing && <h2>Créer une image</h2> || <h2>Editer l'image N° {props.datatId}</h2>}
            <form className={classes.root} autoComplete="on" onSubmit={handleSubmit}>
                <FormControl id="myform" >
                    <div>

                        <Box component="div" display="inline" p={1} m={1}>
                            <Input id="nomImage-input" name="nomImage" aria-describedby="my-helper-text" placeholder="Nom de l'image"
                                onChange={handleChange(setImage, image)} value={image.nomImage} />
                        </Box>

                        <Box component="div" display="inline" p={0} m={0}>

                            <Input
                                type="file"
                                accept="image/*"
                                className={classes.input}
                                id="contained-button-file"
                                type="file"
                                name="urlImage"
                                onChange={(e) => { handleUpload(setImage, image, e); UpLoad(e) }}
                                encType="multipart/from-data"
                            />
                        </Box>

                        <Box component="div" display="inline" p={0} m={0}>
                            <label htmlFor="contained-button-file">
                                <Button variant="contained" component="span" className={classes.button}>Charger une image</Button>
                            </label>
                        </Box>

                        <Box component="div" display="inline" p={1} m={1}>

                            {!editing &&
                                <Fab type="submit" size="small" color="primary" aria-label="Add" className={classes.margin}
                                    onClick={() => { PushValues(image, false); Create('Create'); SentUpLoadFile() }}  > <AddIcon /> </Fab>
                                ||
                                <Box component="div" display="inline" p={1} m={1}>
                                    <Fab type="submit" size="small" color="secondary" aria-label="Add" className={classes.margin}
                                        onClick={() => {
                                            if (window.confirm('Vous allez modifier ID= ' + props.datatId + ' etes vous sur ?')) {
                                                PushValues(image, false);
                                            } else { onCancel() }
                                        }}> <Edit /> </Fab>

                                    <Fab type="submit" size="small" color="secondary" aria-label="Add" className={classes.margin}
                                        onClick={() => { onCancel(); setEditing(false) }}> <Backspace /> </Fab>
                                </Box>
                            }
                        </Box>
                    </div>
                </FormControl>
            </form>
        </React.Fragment>
    )
}