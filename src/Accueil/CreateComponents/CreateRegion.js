
import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Backspace from '@material-ui/icons/Backspace';
import Input from '@material-ui/core/Input';
import Select from '@material-ui/core/Select';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Edit from '@material-ui/icons/Edit';
import DialogTitle from '@material-ui/core/DialogTitle';
import useForm from './useForm';

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },

    margin: {
        margin: theme.spacing(1),
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));
export default function CreateRegion(props) {
    const classes = useStyles();
    const [editing, setEditing] = React.useState(false);
    const { handleChange, handleSubmit } = useForm();
    const [region, setRegion] = React.useState({
        nomRegion: '',
        image: ''
    });
    const [dataImage, setDataImage] = React.useState();
    const [state, setState] = React.useState({ open: false });
    const [iri, setIri] = React.useState('/api/images/')

    function handleClickOpen() {
        setState({ ...state, open: true });
    }

    function handleClose() {
        setState({ ...state, open: false });
    }

    function PushValues(values, boolean) {
        props.SentValues(values)
        props.SentLoad(boolean)
        setEditing(false)
    }

    function Create(action) {
        props.SentCreate(action)
        setRegion({
            nomRegion: '',
            Image: ''
        })
    }



    function onCancel() {
        setRegion({
            nomRegion: '',
            Image: ''
        })
        props.onCancel()
        setEditing(false)
    }
    function getImage() {
        Axios.get('http://localhost:8000/Image/').then(response => {
            const data = response.data
            setDataImage(data)
        })
    }
    function getRegion(id) {
        try {
            Axios.get(props.urlList.concat(id))
                .then(response => {
                    console.log('nom image ' + response.data.nomRegion)
                    setRegion(response.data)
                });
        } catch (error) {
            console.log(error.response)
        }
    }
    useEffect((e) => {
        if (props.dataId !== null, props.action === "Update") {
            setEditing(true);
            getRegion(props.datatId)
        }

    }, [props.action] && [props.datatId]);

    return (
        <React.Fragment>
            {console.log(region.image)}
            {!editing && <h2>Créer une region</h2> || <h2>Editer la region N° {props.datatId}</h2>}
            <form className={classes.root} autoComplete="on" onSubmit={handleSubmit}>
                <FormControl id="myform" >
                    <div>
                        <Box component="div" display="inline" p={1} m={1}>
                            <Input id="my-input" aria-describedby="my-helper-text" placeholder="nom de région" name='nomRegion'
                                onChange={handleChange(setRegion, region)} value={region.nomRegion} />
                        </Box>

                        <Box component="div" display="inline" p={1} m={1}>
                            <Button onClick={() => { handleClickOpen(); getImage() }}>Associer une image à votre région</Button>

                            <Dialog disableBackdropClick disableEscapeKeyDown open={state.open} onClose={handleClose}>
                                <DialogTitle>selectionner une image</DialogTitle>

                                <DialogContent>
                                    <form className={classes.container}>
                                        <FormControl className={classes.formControl}>
                                            <InputLabel htmlFor="age-native-simple">Image</InputLabel>
                                            <Select native value={state.age} name='image' onChange={handleChange(setRegion, region)} input={<Input id="theme" />}>
                                                <option value="" />
                                                {dataImage && dataImage.map(Image => (
                                                    <option key={Image.id} name='image' value={iri.concat(Image.id)}>{Image.nomImage}</option>


                                                ))}
                                            </Select>
                                        </FormControl>
                                    </form>
                                </DialogContent>

                                <DialogActions>
                                    <Button onClick={handleClose} color="primary">Cancel</Button>
                                    <Button onClick={handleClose} color="primary">Ok</Button>
                                </DialogActions>

                            </Dialog>
                        </Box>

                        <Box component="div" display="inline" p={1} m={1}>

                            {!editing &&
                                <Fab type="submit" size="small" color="primary" aria-label="Add" className={classes.margin}
                                    onClick={() => { PushValues(region, false); Create('Create'); }}  > <AddIcon /> </Fab>
                                ||
                                <Box component="div" display="inline" p={1} m={1}>
                                    <Fab type="submit" size="small" color="secondary" aria-label="Add" className={classes.margin}
                                        onClick={() => {
                                            if (window.confirm('Vous allez modifier ID= ' + props.datatId + ' etes vous sur ?')) {
                                                PushValues(region, false);
                                            } else { onCancel() }
                                        }}> <Edit /> </Fab>

                                    <Fab type="submit" size="small" color="secondary" aria-label="Add" className={classes.margin}
                                        onClick={() => { onCancel(); setEditing(false) }}> <Backspace /> </Fab>
                                </Box>
                            }
                        </Box>
                    </div>
                </FormControl>
            </form>
        </React.Fragment>
    )
}
