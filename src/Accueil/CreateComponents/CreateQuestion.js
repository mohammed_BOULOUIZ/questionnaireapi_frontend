import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import FormControl from '@material-ui/core/FormControl';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import Select from '@material-ui/core/Select';
import useForm from './useForm';
import Edit from '@material-ui/icons/Edit';
import Backspace from '@material-ui/icons/Backspace';
import MenuItem from '@material-ui/core/MenuItem';

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },

    margin: {
        margin: theme.spacing(1),
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        maxWidth: 300,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

export default function CreateQuestion(props) {
    const classes = useStyles();
    const [state, setState] = React.useState({ open: false });
    const { values, handleChange, handleSubmit } = useForm();
    const [editing, setEditing] = React.useState(false);
    const [question, setQuestion] = React.useState({
        contenu: '',
        questionnaire: '',
        typereponse: [],
        themequestion: ''
    });
    const [iriQuestionnaire, setIriQuestionnaire] = React.useState('/api/questionnaires/')
    const [iriTheme, setIriTheme] = React.useState('/api/theme_questions/')
    const [iriTypeReponse, setIriTypeReponse] = React.useState('/api/type_reponses/')
    const [dataQuestionnaire, setDataQuestionnaire] = React.useState();
    const [dataTypeReponse, setDataTypeReponse] = React.useState([]);
    const [dataTheme, setDataTheme] = React.useState();


    function handleClickOpen() {
        setState({ ...state, open: true });
    }

    function handleClose() {
        setState({ ...state, open: false });
    }

    function PushValues(values, boolean) {
        console.log(values);
        props.SentValues(values);
        props.SentLoad(boolean)
        setEditing(false)
    }

    function Create(action) {
        props.SentCreate(action)
        setQuestion({
            contenuQuestion: '',
            themequestion: '',
            typereponse: [],
            questionnaire: ''
        })
    }


    function onCancel() {
        setQuestion({
            contenu: '',
            themequestion: '',
            typereponse: [],
            questionnaire: ''
        })
        props.onCancel()
        setDataTypeReponse()
        setEditing(false)
    }

    

    function getTypeReponse() {
        Axios.get('http://localhost:8000/TypeReponse/').then(response => {
            setDataTypeReponse(response.data)
        });
    }
    function getQuestionnaire() {
        Axios.get('http://localhost:8000/Questionnaire/').then(response => {
            setDataQuestionnaire(response.data)
        });
    }
    function getTheme() {
        Axios.get('http://localhost:8000/Theme/').then(response => {
            setDataTheme(response.data)
            console.log(response.data)
        });
    }

    

    function getQuestion(id) {
        Axios.get(props.urlList.concat(id)).then(response => {
            setQuestion({
            contenu: response.data.contenu,
            themequestion: response.data.themequestion,
            typereponse: [],
            questionnaire: response.data.questionnaire
            })
            console.log(response.data)
        });
    }

    useEffect((e) => {
        if (props.dataId !== null, props.action === "Update") {
            setEditing(true);
            getQuestion(props.datatId)
        }
    }, [props.action] && [props.datatId]);

    return (
        <React.Fragment>
            {!editing && <h2>Créer une question</h2> || <h2>Editer la question N° {props.datatId}</h2>}
            <form className={classes.root} autoComplete="on" onSubmit={handleSubmit}>
                <FormControl id="myform" onSubmit={handleSubmit}>
                    <div>
                        <Box component="div" display="inline" p={1} m={1}>
                            <TextField id="filled-full-width" label="Contenu de la question" style={{ margin: 8 }}
                                placeholder="Etes vous satisfait ?" helperText="Ecrivez une question" fullWidth margin="normal" variant="filled" InputLabelProps={{ shrink: true, }}
                                name="contenu" onChange={handleChange(setQuestion, question)} value={question.contenu}
                            />
                        </Box>

                        <Box component="div" display="inline" p={1} m={1}>
                            <Button onClick={() => { handleClickOpen(); getTheme(); getQuestionnaire(); getTypeReponse() }}>Associer un theme/type de reponse/questionnaire</Button>

                            <Dialog disableBackdropClick disableEscapeKeyDown open={state.open} onClose={handleClose}>

                                <DialogTitle>Chaque question devra etre liée à un theme et un questionnaire</DialogTitle>

                                <DialogContent>
                                    <form className={classes.container}>

                                        <FormControl className={classes.formControl}>
                                            <InputLabel htmlFor="themequestion">Theme</InputLabel>
                                            <Select native
                                                value={question.themequestion}
                                                name="themequestion"
                                                onChange={handleChange(setQuestion, question)}
                                                input={<Input id="themequestion" />}>
                                                <option value="" />
                                                {dataTheme && dataTheme.map(Theme => (
                                                    <option key={Theme.id} value={iriTheme.concat(Theme.id)} >{Theme.nomTheme}</option>
                                                ))}
                                            </Select>
                                        </FormControl>

                                        <FormControl className={classes.formControl}>
                                            <InputLabel htmlFor="typereponse" >Type de reponse</InputLabel>
                                            <Select multiple
                                                value={question.typereponse}
                                                name="typereponse"
                                                onChange={handleChange(setQuestion, question)}
                                                input={<Input id="typereponse" />}
                                                MenuProps={MenuProps}
                                            >
                                                {dataTypeReponse && dataTypeReponse.map(typeReponse => (
                                                    <MenuItem key={typeReponse.id} value={iriTypeReponse.concat(typeReponse.id)}>{typeReponse.nomTypeReponse}</MenuItem>
                                                ))}

                                                
                                            </Select>
                                        </FormControl>

                                        <FormControl className={classes.formControl}>
                                            <InputLabel htmlFor="Questionnaire">Questionnaire</InputLabel>
                                            <Select native
                                                value={question.questionnaire}
                                                name="questionnaire" onChange={handleChange(setQuestion, question)}
                                                input={<Input id="questionnaire" />}>
                                                <option value="" />
                                                {dataQuestionnaire && dataQuestionnaire.map(Questionnaire => (
                                                    <option key={Questionnaire.id} value={iriQuestionnaire.concat(Questionnaire.id)} >{Questionnaire.nomQuestionnaire}</option>

                                                ))}
                                            </Select>
                                        </FormControl>
                                    </form>
                                </DialogContent>

                                <DialogActions>
                                    <Button onClick={handleClose} color="primary"> Cancel </Button>
                                    <Button onClick={handleClose} color="primary"> Ok </Button>
                                </DialogActions>
                            </Dialog>
                        </Box>

                        <Box component="div" display="inline" p={1} m={1}>
                            {!editing &&
                                <Fab type="submit" size="small" color="primary" aria-label="Add" className={classes.margin}
                                    onClick={() => { PushValues(question, false); Create('Create'); }}  > <AddIcon /> </Fab>
                                ||
                                <Box component="div" display="inline" p={1} m={1}>
                                    <Fab type="submit" size="small" color="secondary" aria-label="Add" className={classes.margin}
                                        onClick={() => {
                                            if (window.confirm('Vous allez modifier ID= ' + props.datatId + ' etes vous sur ?')) {
                                                PushValues(question, false);
                                            } else { onCancel() }
                                        }}> <Edit /> </Fab>

                                    <Fab type="submit" size="small" color="secondary" aria-label="Add" className={classes.margin}
                                        onClick={() => { onCancel(); setEditing(false) }}> <Backspace /> </Fab>
                                </Box>
                            }
                        </Box>
                    </div>
                </FormControl>
            </form>
        </React.Fragment>
    )
}
