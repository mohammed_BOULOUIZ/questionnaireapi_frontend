import React, { useState, useEffect } from 'react';
import CreateRegion from './CreateComponents/CreateRegion';
import CreateImage from './CreateComponents/CreateImage';
import CreateTheme from './CreateComponents/CreateTheme';
import CreateQuestionnaire from './CreateComponents/CreateQuestionnaire';
import CreateQuestion from './CreateComponents/CreateQuestion';
import CreateTypeReponse from './CreateComponents/CreateTypeReponse';

export default function ButtonCreate(props) {
  const [action, setAction] = React.useState();
  const [id, setId] = React.useState();

  function SentValues(values) {
    props.TransitValues(values)
  }

  function SentCreate(action){
    props.TransitCreate(action)
  }

  function SentLoad(boolean){
    props.TransitLoad(boolean)
  }


  
  if (props.selectionSwitch && props.selectionSwitch === '/Questionnaire/') {
    return (
      <CreateQuestionnaire urlList={props.urlList} SentValues={SentValues} SentCreate={SentCreate}
      datatId={props.datatId} action={props.action} onCancel={props.onCancel} SentLoad={SentLoad} />
    )
  } else
    if (props.selectionSwitch && props.selectionSwitch === '/Question/') {
      return (
        <CreateQuestion urlList={props.urlList} SentValues={SentValues} SentCreate={SentCreate}
        datatId={props.datatId} action={props.action} onCancel={props.onCancel} SentLoad={SentLoad} />
      )
    } else
      if (props.selectionSwitch && props.selectionSwitch === '/Theme/') {
        return (
          <CreateTheme urlList={props.urlList} SentValues={SentValues} SentCreate={SentCreate}
          datatId={props.datatId} action={props.action} onCancel={props.onCancel} SentLoad={SentLoad}/>
        )
      } else
        if (props.selectionSwitch && props.selectionSwitch === '/TypeReponse/') {
          return (
            <CreateTypeReponse urlList={props.urlList} SentValues={SentValues} SentCreate={SentCreate}
            datatId={props.datatId} action={props.action} onCancel={props.onCancel} SentLoad={SentLoad}  />
          )
        } else
          if (props.selectionSwitch && props.selectionSwitch === '/Region/') {
            return (
              <CreateRegion urlList={props.urlList} SentValues={SentValues} SentCreate={SentCreate}
              datatId={props.datatId} action={props.action} onCancel={props.onCancel} SentLoad={SentLoad}  />
            )
          } else
            if (props.selectionSwitch && props.selectionSwitch === '/Image/') {
              return (
                <CreateImage urlList={props.urlList} SentValues={SentValues} SentCreate={SentCreate}
                datatId={props.datatId} action={props.action} onCancel={props.onCancel} SentLoad={SentLoad}/>
              )
            }
            else { return null }
};
