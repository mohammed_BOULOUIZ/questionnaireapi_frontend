

import React, {Fragment} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import DataTableHead from './DataTableHead';
import DataTableBody from './DataTableBody'
import CreateComponent from './CreateComponents';


const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  paper: {
    marginTop: theme.spacing(3),
    width: '100%',
    overflowX: 'auto',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 500,
  },
}));


export default function DataTable(props) {
  const classes = useStyles();
  const dataProps = props.dataContent
  const selectionSwitch = props.selectionSwitch
  const urlList = props.urlList



  function TransitId(id) {
    props.RecievId(id)
  }

  function TransitAction(action) {
    props.ReceiveAction(action)
  }

  function TransitValues(values) {
    props.ReciveValues(values)
  }

  function TransitCreate(action) {
    props.receiveCreate(action)
  }

  function TransitLoad(boolean) {
    props.onLoad(boolean)
  }


  if (typeof(dataProps) === 'undefined' || dataProps.length===0) {
    console.log(dataProps)
    return (
      <React.Fragment>
        <Box color="text.primary" clone>

          <CreateComponent selectionSwitch={selectionSwitch} urlList={urlList}
            TransitValues={TransitValues} TransitAction={TransitAction} TransitCreate={TransitCreate}
            datatId={props.datatId} action={props.action} onCancel={props.onCancel} TransitLoad={TransitLoad} />

        </Box>
    
      <h1>la base est vide veuillez la remplir en utilisant le formulaire ci-dessous...</h1>

      </React.Fragment>
    );
    
  } else {
    console.log(dataProps)

    return (
      <Fragment>

        <Box color="text.primary" clone>
          <CreateComponent selectionSwitch={selectionSwitch} urlList={urlList}
            TransitValues={TransitValues} TransitAction={TransitAction} TransitCreate={TransitCreate}
            datatId={props.datatId} action={props.action} onCancel={props.onCancel} TransitLoad={TransitLoad} />
        </Box>

        <div className={classes.root}>
          <Paper className={classes.paper}>
            <Table className={classes.table} size="small">
              <DataTableHead rowsAttribute={dataProps} />
              <DataTableBody rowsData={dataProps}
                TransitId={TransitId} TransitAction={TransitAction} TransitLoad={TransitLoad} />
            </Table>
          </Paper>
        </div>

      </Fragment>

    ) 
    
  }
  
}
