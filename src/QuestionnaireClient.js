import React, { Component, useState } from 'react';
import Rating from '@material-ui/lab/Rating';
import Axios from 'axios';
import { withStyles, makeStyles, Paper, Grid, Container, RadioGroup, Radio, TextField, Box, Fab, Typography, FormControl, FormControlLabel, Hidden } from '@material-ui/core';
import { red, green } from '@material-ui/core/colors';
import PropTypes from 'prop-types';
import Tooltip from '@material-ui/core/Tooltip';

const useStyles = makeStyles(theme => ({
    container: {
        flexWrap: 'wrap',
    },
    margin: {
        margin: theme.spacing(),
    },
    extendedIcon: {
        marginRight: theme.spacing(3),
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    button: {
        margin: theme.spacing(1),
    },
    input: {
        display: 'none',
    },

    rating1: {
        width: 200,
        display: 'flex',
        alignItems: 'center',
    },
    Paper: {
        display: 'container',


    },

    Div: {
        height: 'auto',
        width: 'auto',
        position: 'absolute',
        left: '200px',
        border: 'dotted',
        display: 'inline-block',
        'padding-left': '100px',
        'padding-right': '100px'
    },

    Box: {
        'padding-top': '5px',
        paddingBottom: '20px',
        width: 'auto',

    },

    h6: {

    },

    h1: {
        'align': 'center',
    },

    Radio1: {
        'padding-right': '30px',
        'color': 'green'
    },
    Radio2: {
        'padding-right': '30px',
        'color': 'red'
    },

    Tooltip: {

    }

}));






export default function QuestionnaireClient() {
    const classes = useStyles();
    const [lenght, setLenght] = React.useState(0);
    const [valueRadio, setValueRadio] = React.useState();
    const [valueRate, setValueRate] = React.useState();
    const [valueComment, setValueComment] = React.useState();
    const [listId, setListId] = React.useState();
    const [qClient, setQClient] = useState(
        {
            nomQuestionnaire: '',
            questions: [{
                id: '',
                contenu: '',
                typereponse: [
                    { nomTypeReponse: '' }
                ],
                themequestion: { nomTheme: '' },
            }],
            regions: []
        })

    const [repClient, setRepClient] = useState({ valueRadio: '', valueRate: '', valueComment: '' })

    const labels = {
        1: 'Très insatisfait',
        2: 'insatisfait',
        3: 'satisfait',
        4: 'Très satisfait',
    };

    function IconContainer(props) {
        const { value, ...other } = props;
        return (
            <Tooltip className={classes.Tooltip} title={labels[value] || ''}>
                <div {...other} />
            </Tooltip>
        );
    }

    IconContainer.propTypes = {
        value: PropTypes.number.isRequired,
    };

    function getQClient() {
        Axios.get('http://localhost:8000/QuestionnaireClient/1')
            .then(response => {
                console.log('questionnaire ' + response.data.Questionnaire)
                setQClient({
                    nomQuestionnaire: response.data.nomQuestionnaire,
                    questions: response.data.questions
                })
                response.data.questions.map(qst => {
                    setListId(listId => ({ ...listId, [qst.id]: qst.id }))
                }
                )

            });
    }

    function sendResponse(event) {
        console.log(valueRadio, valueRate, valueComment)
        event && event.preventDefault();
        if (valueRadio && valueRate){
            Axios.post('http://localhost:8000/QuestionnaireClient/', [valueRadio, valueRate, valueComment])
                .then(response => {
                    console.log('la reponse client ' + response)
                });
        }else{
            alert('il y a des champs manquant, veuillez svp les compléter');
        }

    }

    function onChange(event, questionID) {
        var vRadio = event.target.value;
        var vRate = event.target.value;
        var vComment = event.target.value;

        setValueRadio(valueRadio => ({ ...valueRadio, [questionID]: vRadio }));
        setValueRate(valueRate => ({ ...valueRate, [questionID]: vRate }));
        setValueComment(valueComment => ({ ...valueComment, [questionID]: vComment }));
    }

    return (
        <div className={classes.Div}>
            <Grid className={classes.Grid} pl={5} pr={5}>
                <h1 className={classes.h1}>{qClient && qClient.nomQuestionnaire}</h1>
                <Fab type="submit" size="small" color="secondary" aria-label="Add" className={classes.margin} onClick={() => { getQClient() }}></Fab>

                <form className={classes.root} onSubmit={sendResponse}>
                    <FormControl id="myform" onSubmit={sendResponse}>
                        {qClient.questions && qClient.questions.map(question =>

                            <Box className={classes.Box} mb={5}>
                                <h6 className={classes.h6}>{question && question.contenu}</h6>
                                {question && question.typereponse.map(type => {

                                    if (type.nomTypeReponse == 'reponse_directe') {
                                        return (

                                            <Box className={classes.Box} component="fieldset" borderColor="dark" >
                                                <RadioGroup aria-label="position" name={'O/N de la Qst N°->'+question.id} value={valueRadio && valueRadio['O/N de la Qst N°->'+question.id]}
                                                    onChange={(e) => onChange(e, 'O/N de la Qst N°->'+question.id)} row >
                                                    <FormControlLabel
                                                    
                                                        value="oui"
                                                        control={<Radio color="primary" required="required"/>}
                                                        label="oui"
                                                        labelPlacement="start"
                                                        className={classes.Radio1}
                                                    />
                                                    <FormControlLabel
                                                    
                                                        value="non"
                                                        control={<Radio color="primary" required="required"/>}
                                                        label="non"
                                                        labelPlacement="end"
                                                        className={classes.Radio2}
                                                    />
                                                </RadioGroup>
                                            </Box>
                                        )
                                    }
                                    if (type.nomTypeReponse == 'reponse_multiple') {
                                        return (
                                            <Box className={classes.Box} component="fieldset" borderColor="transparent">
                                                <Rating
                                                    name={'nbr etoiles pour la Qst N°->'+question.id}
                                                    value={valueRate && valueRate['nbr etoiles pour la Qst N°->'+question.id]}
                                                    rating={onChange} 
                                                    onChange={(e) => onChange(e, 'nbr etoiles pour la Qst N°->'+question.id)}
                                                    max={4}
                                                    IconContainerComponent={IconContainer}
                                                />
                                            </Box>
                                        )
                                    }
                                    if (type.nomTypeReponse == 'reponse_commentaire') {
                                        return (

                                            <Box className={classes.Box} component="fieldset" borderColor="transparent">
                                                <TextField
                                                    label="Commentaire"
                                                    style={{ margin: 'auto' }}
                                                    placeholder="Votre avis nous intérésse...!"
                                                    fullWidth
                                                    name={'Commentaire de la Qst N°->'+question.id}
                                                    variant="outlined"
                                                    InputLabelProps={{
                                                        shrink: true,
                                                    }}
                                                    onChange={(e) => onChange(e, 'Commentaire de la Qst N°->'+question.id)}
                                                />
                                            </Box>
                                        )
                                    }
                                })}
                            </Box>
                        )}
                        <Fab type="submit" size="small" color="primary" aria-label="Add" className={classes.margin} onSubmit={(e) => sendResponse(e)}></Fab>
                    </FormControl>
                </form>
            </Grid>
        </div>


    )

}