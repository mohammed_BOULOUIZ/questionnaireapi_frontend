import React, { Component, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import Input from '@material-ui/core/Input';



const useStyles = makeStyles(theme => ({
    button: {
        display: 'block',
        marginTop: theme.spacing(2),
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
}));


export default function Julien() {
    const classes = useStyles();
    const [age, setAge] = React.useState('');
    const [open, setOpen] = React.useState(false);

    function handleChange(event) {
        setAge(event.target.value);
    }

    function handleClose() {
        setOpen(false);
    }

    function handleOpen() {
        setOpen(true);
    }


    return (
        <React.Fragment>
            <Box>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="Theme"> <NavigateNextIcon /></InputLabel>
                    <Select native value="{question.themequestion}" name='themequestion' input={<Input id="themequestion" />}>
                        <option value="" />
                        <option value="">coucou</option>
                        <option value="">coucou</option>
                    </Select>
                </FormControl>
            </Box>
            <Box>
                <Button className={classes.button} onClick={handleOpen}>
                    <NavigateNextIcon /> <KeyboardArrowRightIcon />
                </Button>
                <FormControl className={classes.formControl}>
                    <Select
                        open={open}
                        onClose={handleClose}
                        onOpen={handleOpen}
                        value={age}
                        onChange={handleChange}
                        inputProps={{
                            name: 'age',
                            id: 'demo-controlled-open-select',
                        }}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                    </Select>
                </FormControl>
            </Box>
        </React.Fragment>
    )
}